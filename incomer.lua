script_name("Incomer")
script_authors("Kinder (Antonio_Lucchetti)")
script_version("1.0.1")

require "moonloader"
local sampev = require 'lib.samp.events'
local imgui = require "imgui"
local inicfg = require "inicfg"
local memory = require "memory"
local encoding = require 'encoding'
encoding.default = 'CP1251'
utf8Encoding = encoding.UTF8

local json_url = "https://gitlab.com/RTHeLL/samp-incomer/-/raw/main/version.json"
STATUSEX_ENDDOWNLOAD = 58
STATUS_ENDDOWNLOADDATA = 6

local tag = "[ Incomer ] {FFFFFF}"
local mainColor, secondaryColor = "{E3B900}", "{B3B3B3}"
local mainColorHex = 0xE3B900

local incomeTypes = {
	[0] = 'day',
	[1] = 'session'
}

local configData = inicfg.load({
	main = {
		isAutoUpdate = true,
        isShowInfoWindow = true,
		incomeType = incomeTypes[0],
		infoWindowPosX = select(1, getScreenResolution()) - 250,
		infoWindowPosY = select(2, getScreenResolution()) / 2
	}
}, "incomer/incomerConfig")

local budgetStatistic = {
    main = {
		day = os.date("%d%m%Y"),
        earned = 0,
		spent = 0,
		total = 0
    }
}

local isAutoUpdateImGUI = imgui.ImBool(configData.main.isAutoUpdate)


function GetPlayerMoney()
	while not isSampAvailable() do wait(0) end

	while not sampIsLocalPlayerSpawned() do
		wait(0)
	end

	return getPlayerMoney(PLAYER_HANDLE)
end


local oldMoney = nil


function log(text, tag)
	local output = string.format("%s[%s]: {FFFFFF}%s", secondaryColor, (tag or "Incomer Debug"), text)
	local result = pcall(sampfuncsLog, output)
	if not result then print(output) end
end


function main()
    while not isSampAvailable() do wait(0) end

	oldMoney = GetPlayerMoney()

	imgui.Process = true
    imgui.ShowCursor = false

	if isAutoUpdateImGUI.v then CheckUpdate() end

    init()
end


function init()
    if not CheckConfigFile() then SaveOrCreateConfigFile() end
	-- if not CheckBudgetFile() then SaveOrCreateStatFile() end
    
	-- if configData.main.incomeType == incomeTypes[0] then
	-- if budgetStatistic.main.day == os.date("%d%m%Y") then
	-- 	budgetStatistic.main.day = os.date("%d%m%Y")
	-- 	budgetStatistic.main.earned = 0
	-- 	budgetStatistic.main.spent = 0
	-- end
	-- end

    RegisterSAMPCommands()

    SendMessage("{FFFFFF}������ �������� ������ ������� ��������!")
    SendMessage("{FFFFFF}�����: {E3B900}Kinder (Antonio_Lucchetti)")
end


function sampev.onSendStatsUpdate(money)
	lua_thread.create(function()

		if oldMoney then
			if oldMoney < money then
				budgetStatistic.main.earned = budgetStatistic.main.earned + (money - oldMoney)
				oldMoney = money
			elseif oldMoney > money then
				budgetStatistic.main.spent = budgetStatistic.main.spent + (oldMoney - money)
				oldMoney = money
			end
		end

		budgetStatistic.main.total = budgetStatistic.main.earned - budgetStatistic.main.spent
	end)
end


function RegisterSAMPCommands()
    sampRegisterChatCommand('incomer', function()
		ChangeInfoWindowPosition()
	end)
end


function SendMessage(message, color)
	message = message:gsub('{MAIN}', mainColor)
	message = message:gsub('{WHITE}', "{FFFFFF}")
	message = message:gsub('{SECONDARY}', secondaryColor)
	sampAddChatMessage(tag .. message, color or mainColorHex)
end


function CheckConfigFile()
    if not doesFileExist('moonloader/config/incomer/incomerConfig.ini') then
		return false
	end
    return true
end


function CheckBudgetFile()
	if not doesFileExist('moonloader/config/incomer/incomerDayStat.ini') then
		return false
	end
    return true
end


function SaveOrCreateStatFile()
    if inicfg.save(budgetStatistic, 'incomer/incomerDayStat.ini') then log('���� ������������ <incomerDayStat.ini> ��������') end
end


function SaveOrCreateConfigFile()
    if inicfg.save(configData, 'incomer/incomerConfig.ini') then log('���� ������������ <incomerConfig.ini> ��������') end
end


function imgui.OnDrawFrame()
  GetIncomeInfo()
end


function onWindowMessage(msg, wparam, lparam)
	if process_position then
		if msg == 0x0201 then -- LButton
			SendMessage("����������������� ���������!")
			process_position = nil; consumeWindowMessage(true, true)
		elseif msg == 0x0100 and wparam == 0x1B then -- Esc
			configData.main.infoWindowPosX = process_position[2]
			configData.main.infoWindowPosY = process_position[3]
			SendMessage('�� �������� ��������� �����������������')
			process_position = nil; consumeWindowMessage(true, true)
		end
	end
end


function onScriptTerminate(script, quitGame)
	if script == thisScript() then
		if not sampIsDialogActive() then
			showCursor(false, false)
		end
		SaveOrCreateConfigFile()
		SaveOrCreateStatFile()
	end
end


function SplitARGB(argb)
	local a = bit.band(bit.rshift(argb, 24), 0xFF)
	local r = bit.band(bit.rshift(argb, 16), 0xFF)
	local g = bit.band(bit.rshift(argb, 8), 0xFF)
	local b = bit.band(argb, 0xFF)
	return a, r, g, b
end


function imgui.TextColoredRGB(text)
	local style = imgui.GetStyle()
	local colors = style.Colors
	local ImVec4 = imgui.ImVec4

	local getcolor = function(color)
		if color:sub(1, 6):upper() == 'SSSSSS' then
			local r, g, b = colors[1].x, colors[1].y, colors[1].z
			local a = tonumber(color:sub(7, 8), 16) or colors[1].w * 255
			return ImVec4(r, g, b, a / 255)
		end
		local color = type(color) == 'string' and tonumber(color, 16) or color
		if type(color) ~= 'number' then return end
		local r, g, b, a = SplitARGB(color)
		return imgui.ImColor(r, g, b, a):GetVec4()
	end

	local render_text = function(text_)
		for w in text_:gmatch('[^\r\n]+') do
			local text, colors_, m = {}, {}, 1
			w = w:gsub('{(......)}', '{%1FF}')
			while w:find('{........}') do
				local n, k = w:find('{........}')
				local color = getcolor(w:sub(n + 1, k - 1))
				if color then
					text[#text], text[#text + 1] = w:sub(m, n - 1), w:sub(k + 1, #w)
					colors_[#colors_ + 1] = color
					m = n
				end
				w = w:sub(1, n - 1) .. w:sub(k + 1, #w)
			end
			if text[0] then
				for i = 0, #text do

					imgui.TextColored(colors_[i] or colors[1], utf8Encoding(text[i]))
					imgui.SameLine(nil, 0)
				end
				imgui.NewLine()
			else imgui.Text(utf8Encoding(w)) end
		end
	end

	render_text(text)
end


function GetIncomeInfo()
	-- if not (budgetStatistic.main.day == os.date("%d%m%Y")) then
	-- 	budgetStatistic.main.day = os.date("%d%m%Y")
	-- 	budgetStatistic.main.earned = 0
	-- 	budgetStatistic.main.spent = 0
	-- end
	
	if configData.main.isShowInfoWindow and IsHudVisible() then
		imgui.SetNextWindowPos(imgui.ImVec2(configData.main.infoWindowPosX, configData.main.infoWindowPosY), imgui.ImVec2(0.5, 0.5))
		imgui.SetNextWindowSize(imgui.ImVec2(170, 115), imgui.Cond.FirstUseEver)
		imgui.PushStyleVar(imgui.StyleVar.WindowPadding, imgui.ImVec2(0, 0))
		imgui.PushStyleVar(imgui.StyleVar.WindowRounding, 5)
		imgui.Begin(utf8Encoding("������"), _, imgui.WindowFlags.NoCollapse + imgui.WindowFlags.NoResize + imgui.WindowFlags.NoMove + imgui.WindowFlags.NoBringToFrontOnFocus + imgui.WindowFlags.NoSavedSettings + imgui.WindowFlags.NoTitleBar)

			local DL = imgui.GetWindowDrawList()
			local p = imgui.GetCursorScreenPos()
			local ws = imgui.GetWindowSize()
			DL:AddRectFilled(
				imgui.ImVec2(p.x + ws.x - 6, p.y), 
				imgui.ImVec2(p.x + ws.x, p.y + ws.y), 
				imgui.ColorConvertFloat4ToU32(imgui.ImVec4(227.0 / 255, 185.0 / 255, 0.0 / 255, 1.00)), 
				imgui.GetStyle().WindowRounding, 
				6
			)
			imgui.SetCursorPos(imgui.ImVec2(8, 8))
            
            imgui.BeginGroup()
                local title = utf8Encoding('������')
                local len = imgui.CalcTextSize(title).x
                DL:AddRectFilled(
                    imgui.ImVec2(p.x, p.y + 5), 
                    imgui.ImVec2(p.x + len + 20, p.y + 25), 
                    imgui.ColorConvertFloat4ToU32(imgui.ImVec4(227.0 / 255, 185.0 / 255, 0.0 / 255, 1.00)), 
                    10, 
                    6
                )

                imgui.SetCursorPosY(7)
                imgui.TextColored(imgui.ImVec4(1, 1, 1, 1), title)

                imgui.SetCursorPosY(35)
                imgui.TextColoredRGB('{FFFFFF}����������: '..SumFormat(budgetStatistic.main.earned)..'$')
                imgui.TextColoredRGB('{FFFFFF}���������: '..SumFormat(budgetStatistic.main.spent)..'$')
                imgui.TextColoredRGB(' ')
                if budgetStatistic.main.total <= 0 then
                    imgui.TextColoredRGB('{FFFFFF}�����: {FF0000}'..SumFormat(budgetStatistic.main.total)..'$')
                else
                    imgui.TextColoredRGB('{FFFFFF}�����: {00B327}'..SumFormat(budgetStatistic.main.total)..'$')
                end
			imgui.EndGroup()

		imgui.End()
		imgui.PopStyleVar(2)
	end
end


function SumFormat(sum)
	sum = tostring(sum)
	if sum and string.len(sum) > 3 then
		local b, e = ('%d'):format(sum):gsub('^%-', '')
		local c = b:reverse():gsub('%d%d%d', '%1.')
		local d = c:reverse():gsub('^%.', '')
		return (e == 1 and '-' or '')..d
	end
	return sum
end


function ChangeInfoWindowPosition()
    lua_thread.create(function ()
        imgui.ShowCursor = true
        while imgui.IsMouseDown(0) do wait(0) end
        process_position = { true, configData.main.infoWindowPosX, configData.main.infoWindowPosY }
        SendMessage('������� {MAIN}���{WHITE} ���-�� ��������� ��������������, ��� {MAIN}ESC{WHITE} ���-�� ��������')
        while process_position ~= nil do
            local x, y = getCursorPos()
            configData.main.infoWindowPosX = x
            configData.main.infoWindowPosY = y
            wait(0)
        end
        imgui.ShowCursor = false
    end)
	SaveOrCreateConfigFile()
end


function IsHudVisible()
	return memory.getint8(0xBA6769) == 1
end


function CheckUpdate()
	local jsonFileLocation = getWorkingDirectory() .. '\\' .. thisScript().name .. '-version.json'
	if doesFileExist(jsonFileLocation) then os.remove(jsonFileLocation) end

	downloadUrlToFile(json_url, jsonFileLocation,
		function(id, status, p1, p2)
			if status == STATUSEX_ENDDOWNLOAD then
				if doesFileExist(jsonFileLocation) then
					local file = io.open(jsonFileLocation, 'r')
					if file then
						local info = decodeJson(file:read('*a'))
						updatelink = info.updateurl
						updateversion = info.latest
						file:close()
						os.remove(jsonFileLocation)

						if updateversion ~= thisScript().version then
							lua_thread.create(function()
								local color = -1
								SendMessage('������� ����������: ' .. thisScript().version .. ' -> ' .. updateversion .. '! ��������...')
								wait(250)
								downloadUrlToFile(updatelink, thisScript().path,
									function(id3, status1, p13, p23)
										if status1 == STATUS_ENDDOWNLOADDATA then
											SendMessage('�������� ��������. ������ �������� �� ������ ' .. mainColor .. updateversion .. '. {FFFFFF} ������������...')
											goupdatestatus = true

											local bAutoReboot = false
											for _, s in ipairs(script.list()) do
												if s.name == "ML-AutoReboot" then
													bAutoReboot = true
												end
											end

											if not bAutoReboot then
												thisScript():reload()
											end
										end
										if status1 == STATUSEX_ENDDOWNLOAD then
											if goupdatestatus == nil then
												SendMessage('������ ��� ����������: �� ��� ������ ����')
												update = false
											end
										end
									end
								)
							end)
						else
							update = false
							SendMessage('���������� �� �������')
						end
					end
				else
					SendMessage('������ ��� ����������: �� ������� �������� JSON �������')
					update = false
				end
			end
		end)
	while update ~= false do wait(100) end
end
